require 'legion/settings/rules'
require 'legion/settings/validators'
require 'legion/settings/constants'

module Legion
  module Settings
    class Validator
      include Rules
      include Validators

      attr_reader :failures

      def initialize
        @failures = []
      end

      def run(settings, service = nil)
        validate_legion(settings[:legion])
        validate_transport(settings[:transport])
        validate_categories(settings)
        case service
        when 'client'
          validate_client(settings[:client])
        when 'api'
          validate_api(settings[:api])
        when 'rspec'
          validate_client(settings[:client])
          validate_api(settings[:api])
        end
        @failures
      end

      def reset!
        failure_count = @failures.size
        @failures = []
        failure_count
      end
      alias reset reset!

      private

      def validate_categories(settings)
        CATEGORIES.each do |category|
          if is_a_hash?(settings[category])
            validate_method = "validate_#{category.to_s.chop}".to_sym
            settings[category].each do |name, details|
              send(validate_method, details.merge(name: name.to_s))
            end
          else
            invalid(settings[category], "#{category} must be a hash")
          end
        end
      end

      def invalid(object, message)
        @failures << {
          object: object,
          message: message
        }
      end
    end
  end
end
