module Legion
  module Settings
    module Rules
      def must_be_a_hash(value)
        value.is_a?(Hash)
      end
      alias is_a_hash? must_be_a_hash

      def must_be_a_hash_if_set(value)
        value.nil? ? true : must_be_a_hash(value)
      end

      def must_be_an_array(value)
        value.is_a?(Array)
      end
      alias is_an_array? must_be_an_array

      def must_be_an_array_if_set(value)
        value.nil? ? true : must_be_an_array(value)
      end

      def must_be_a_string(value)
        value.is_a?(String)
      end
      alias is_a_string? must_be_a_string

      def must_be_a_string_if_set(value)
        value.nil? ? true : must_be_a_string(value)
      end

      def must_be_an_integer(value)
        value.is_a?(Integer)
      end
      alias is_an_integer? must_be_an_integer

      def must_be_an_integer_if_set(value)
        value.nil? ? true : must_be_an_integer(value)
      end

      def must_be_a_numeric(value)
        value.is_a?(Numeric)
      end

      def must_be_a_numeric_if_set(value)
        value.nil? ? true : must_be_a_numeric(value)
      end

      def must_match_regex(regex, value)
        (value =~ regex).zero?
      end

      def must_be_boolean(value)
        !value.nil?
      end

      def must_be_boolean_if_set(value)
        value.nil? ? true : must_be_boolean(value)
      end

      def items_must_be_strings(value, regex = nil)
        value.all? do |item|
          item.is_a?(String) && !item.empty? &&
            (regex.nil? || item =~ regex)
        end
      end

      def either_are_set?(*values)
        values.any? do |value|
          !value.nil?
        end
      end

      def must_be_time(*values)
        values.all? do |value|
          Time.parse(value)
        rescue StandardError
          false
        end
      end

      def must_be_either(allowed, *values)
        values.flatten.all? do |value|
          allowed.include?(value)
        end
      end

      def must_be_either_if_set(allowed, *values)
        values[0].nil? ? true : must_be_either(allowed, values)
      end
    end
  end
end
