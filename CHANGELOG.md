# Legion::Settings Changelog

## v0.2.0
* Adding merge settings method
* Updating default settings

## v0.1.1
* Updating default hash to include legion:{extensions:{}}
* Fixing rubocop errors