lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'legion/settings/version'

Gem::Specification.new do |spec|
  spec.name          = 'legion-settings'
  spec.version       = Legion::Settings::VERSION
  spec.authors       = ['Esity']
  spec.email         = ['matthewdiverson@gmail.com']

  spec.summary       = 'Used to load the json settings files'
  spec.description   = "Based on Sensu's implementation of settings"
  spec.homepage      = 'https://bitbucket.org/whonodes/legion-settings'
  spec.license       = 'MIT'
  spec.required_ruby_version = '>= 2.5.0'

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rspec_junit_formatter'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'simplecov', '< 0.18.0'
  spec.add_dependency 'legion-json'
  spec.add_dependency 'legion-logging'
end
