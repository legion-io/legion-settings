# Legion::Settings

Legion::Settings is part of the Legion Framework

###Badges
####CICD
|         | License | CircleCI | CodeCov | CodeBeat |
|---------|---------|----------|---------|----------|
| Master  |[![MIT Licence](https://badges.frapsoft.com/os/mit/mit.svg?v=103)](https://opensource.org/licenses/mit-license.php)|[![CircleCI](https://circleci.com/bb/legion-io/legion-settings/tree/master.svg?style=svg)](https://circleci.com/bb/legion-io/legion-settings/tree/master)|[![codecov](https://codecov.io/bb/legion-io/legion-settings/branch/master/graph/badge.svg)](https://codecov.io/bb/legion-io/legion-settings)|[![codebeat badge](https://codebeat.co/badges/e86dbe3e-b463-4f3c-91a8-c85492fd3833)](https://codebeat.co/projects/bitbucket-org-legion-io-legion-settings-master)|
| Develop |[![MIT Licence](https://badges.frapsoft.com/os/mit/mit.svg?v=103)](https://opensource.org/licenses/mit-license.php)|[![CircleCI](https://circleci.com/bb/legion-io/legion-settings/tree/develop.svg?style=svg)](https://circleci.com/bb/legion-io/legion-settings/tree/develop)|[![codecov](https://codecov.io/bb/legion-io/legion-settings/branch/develop/graph/badge.svg)](https://codecov.io/bb/legion-io/legion-settings)|[![codebeat badge](https://codebeat.co/badges/e86dbe3e-b463-4f3c-91a8-c85492fd3833)](https://codebeat.co/projects/bitbucket-org-legion-io-legion-settings-develop)|

#####RubyGems
![](https://ruby-gem-downloads-badge.herokuapp.com/legion-settings)
[![Gem Version](https://badge.fury.io/rb/legion-settings.svg)](https://badge.fury.io/rb/legion-settings)

#####Bitbucket
![](https://img.shields.io/bitbucket/issues-raw/legion-io/legion-settings.svg)
![](https://img.shields.io/bitbucket/pr-raw/legion-io/legion-settings.svg)
## Installation

Add this line to your application's Gemfile:

```ruby
gem 'legion-settings'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install legion-settings

## Usage

This library is utilized by Legion to load config files

## Gem

This gem can be viewed and download from [RubyGems - Legion-Settings](https://rubygems.org/gems/legion-settings)

## Contributing

Bug reports and pull requests are welcome on GitHub at https://bitbucket.org/legion-io/legion-settings/issues This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).