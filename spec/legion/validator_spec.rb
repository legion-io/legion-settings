require 'spec_helper'
require File.join(File.dirname(__FILE__), 'helpers')
require 'legion/settings/validator'

RSpec.describe 'Legion::Settings::Validator' do
  include Helpers

  before do
    @validator = Legion::Settings::Validator.new
  end

  # it 'can run, validating setting categories' do
  #   failures = @validator.run({})
  #   expect(failures).to be_kind_of(Array)
  #   failures.each do |failure|
  #     expect(failure[:object]).to be(nil)
  #   end
  #   reasons = failures.map do |failure|
  #     failure[:message]
  #   end
  #   expect(reasons).to include('checks must be a hash')
  #   expect(reasons).to include('filters must be a hash')
  #   expect(reasons).to include('mutators must be a hash')
  #   expect(reasons).to include('handlers must be a hash')
  #   expect(reasons).to include('extensions must be a hash')
  #   expect(reasons.size).to eq(7)
  # end

  it 'can validate a legion definition' do
    legion = nil
    @validator.validate_legion(legion)
    expect(@validator.reset).to eq(1)
    legion = 1
    @validator.validate_legion(legion)
    expect(@validator.reset).to eq(1)
    legion = {}
    @validator.validate_legion(legion)
    expect(@validator.reset).to eq(1)
    legion[:spawn] = 1
    @validator.validate_legion(legion)
    expect(@validator.reset).to eq(1)
    legion[:spawn] = {}
    @validator.validate_legion(legion)
    expect(@validator.reset).to eq(1)
    legion[:spawn][:limit] = '1'
    @validator.validate_legion(legion)
    expect(@validator.reset).to eq(1)
    legion[:spawn][:limit] = 20
    @validator.validate_legion(legion)
    expect(@validator.reset).to eq(0)
  end
end
